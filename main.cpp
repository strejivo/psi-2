#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <set>
#include <unistd.h>
#include <netdb.h>

using namespace std;

// Port na kterem komunikuje server
#define PORT 4000

// delka identifikatoru spojeni v B
#define CONN_ID_LENGTH 4

// delka sekvencniho cisla v B
#define SECV_ID_LENGTH 2

// delka cisla potvrzeni v B
#define CONFIRM_LENGTH 2

// delka priznaku v B
#define SYM_LENGTH 1

// maximalni delka dat v B
#define MAX_DATA_LENGTH 255

// minimalni delka packetu
#define MIN_PACKET_LENGTH CONN_ID_LENGTH+SECV_ID_LENGTH+CONFIRM_LENGTH+SYM_LENGTH+1

// maximálni delka packetu
#define MAX_PACKET_LENGTH CONN_ID_LENGTH+SECV_ID_LENGTH+CONFIRM_LENGTH+SYM_LENGTH+MAX_DATA_LENGTH


// definice priznaku
#define SYM_SYN 4
#define SYM_FIN 2
#define SYM_RST 1

// definice druhu spojeni
#define DOWNLOAD 1
#define UPLOAD 2


// maximalni pocet pokusu
#define MAX_ATTEMPTS 20

// nastaveni timeout hodnot
#define TIMEOUT_SEC 0;
#define TIMEOUT_USEC 100000;

// velikost okenka v B
#define WINDOW_SIZE 2040


/**
 * Vytváří 32 bitové číslo z pole
 * @param data pole ze ktereho vytváří číslo
 * @return Vytvořené číslo
 */
uint32_t createNumber32(unsigned char *data) {
    uint32_t tmp = 0;
    for (size_t i = 0; i < 4; i++) {
        tmp += data[i] * pow(2, 8 * i);
    }
    return tmp;
}

/**
 * Vytváří 16 bitové číslo z pole
 * @param data pole ze ktereho vytváří číslo
 * @return Vytvořené číslo
 */
uint16_t createNumber16(unsigned char *data) {
    uint16_t tmp = 0;
    for (size_t i = 0; i < 2; i++) {
        tmp += data[i] * pow(2, 8 * i);
    }
    return tmp;
}

void createArray(unsigned char *data, uint16_t x) {
    for (uint8_t i = 0; i < 2; i++) data[i] = (x >> 8 * i) & 0xff;
}

void createArray(unsigned char *data, uint32_t x) {
    for (uint8_t i = 0; i < 4; i++) data[i] = (x >> 8 * i) & 0xff;
}

/**
 * Struktura pro data packet
 */
struct dataPacket {
    dataPacket() {
        m_connID = 0;
        m_secvID = 0;
        m_dataLength = 0;
        m_confirmID = 0;
        m_symptom = 0;
    }

    dataPacket(uint32_t m_connID, uint16_t m_secvID, uint16_t m_confirmID, uint8_t m_symptom) : m_connID(m_connID),
                                                                                                m_secvID(m_secvID),
                                                                                                m_confirmID(
                                                                                                        m_confirmID),
                                                                                                m_symptom(
                                                                                                        m_symptom) { m_dataLength = 0; };

    void parseData(unsigned char *data, size_t length) {
        size_t parsed = 0;
        m_connID = ntohl(createNumber32(data + parsed));
        parsed += CONN_ID_LENGTH;
        //cout<<(int)data[parsed]<<"|"<<(int)data[parsed+1]<<endl;
        m_secvID = ntohs(createNumber16(data + parsed));
        parsed += SECV_ID_LENGTH;
        m_confirmID = ntohs(createNumber16(data + parsed));
        parsed += CONFIRM_LENGTH;
        m_symptom = data[parsed];
        parsed += SYM_LENGTH;
        m_dataLength = length - parsed;
        memcpy(m_data, data + parsed, m_dataLength);
    }

    void makeData(unsigned char data[MAX_PACKET_LENGTH], size_t &length) {
        size_t copyed = 0;
        createArray(data + copyed, htonl(m_connID));
        copyed += CONN_ID_LENGTH;
        createArray(data + copyed, htons(m_secvID));
        copyed += SECV_ID_LENGTH;
        createArray(data + copyed, htons(m_confirmID));
        copyed += CONFIRM_LENGTH;
        data[copyed] = m_symptom;
        copyed += SYM_LENGTH;
        length = m_dataLength + copyed;
        memcpy(data + copyed, m_data, m_dataLength);
    }

    friend ostream &operator<<(ostream &x, const dataPacket &d) {
        x << "--------------------------------------" << endl;
        x << "conn ID = " << d.m_connID << endl;
        x << "sevc ID = " << d.m_secvID << endl;
        x << "confirm = " << d.m_confirmID << endl;
        x << "symptom = " << (int) d.m_symptom << endl;
        x << "datalen = " << (int) d.m_dataLength << endl;
        x << "data{" << endl;
        for (size_t i = 1; i < d.m_dataLength; i++) {
            x << (int) d.m_data[i - 1] << "|";
        }
        if (d.m_dataLength > 0) x << (int) d.m_data[d.m_dataLength - 1] << endl << "}" << endl;
        x << "-------------------------------------" << endl;
        return x;
    }

    bool operator<(const dataPacket &x) const {
        return m_secvID < x.m_secvID;
    }

    uint32_t m_connID;
    uint16_t m_secvID;
    uint16_t m_confirmID;
    uint8_t m_symptom;
    unsigned char m_data[MAX_DATA_LENGTH];
    uint8_t m_dataLength;

};

/**
 * Vytvoří a vyplní data packet, kterým se vytvoří spojení
 * @param mode Druh spojení (upload = UPLOAD, download = DOWNLOAD)
 * @return vyplněný data packet
 */
dataPacket createConnPacket(unsigned char mode) {
    dataPacket tmp;
    tmp.m_connID = 0;
    tmp.m_secvID = 0;
    tmp.m_confirmID = 0;
    tmp.m_symptom = SYM_SYN;
    tmp.m_data[0] = mode;
    tmp.m_dataLength = 1;
    return tmp;
}

dataPacket createAckPacket(uint32_t connID, uint16_t secvID) {
    dataPacket recv;
    recv.m_connID = connID;
    recv.m_secvID = 0;
    recv.m_dataLength = 0;
    recv.m_confirmID = secvID;
    recv.m_symptom = 0;
    return recv;
}

dataPacket createRSTPacket(uint32_t connID){
    return dataPacket(connID, 0, 0, SYM_RST);
}

int createUDPSocket(string name, string port, addrinfo *&ai) {
    int sock;
    if (getaddrinfo(name.c_str(), port.c_str(), NULL, &ai) != 0) {
        return -1;
    }
    if ((sock = socket(ai->ai_family, SOCK_DGRAM, IPPROTO_UDP)) < 0) return -1;
    if (connect(sock, ai->ai_addr, ai->ai_addrlen) == -1) {
        return -1;
    }
    return sock;
}

void sendRSTPacket(int socket, addrinfo * ai, uint32_t connID){
    unsigned char buffer [MAX_PACKET_LENGTH];
    size_t l;
    createRSTPacket(connID).makeData(buffer, l);
    sendto(socket, buffer, l, 0, ai->ai_addr, ai->ai_addrlen);
}

bool initiateConnection(int socket, addrinfo *ai, int mode, uint32_t &connID) {
    cout << "INITIATING CONNECTION" << endl;
    unsigned char sendHeader[MIN_PACKET_LENGTH];
    unsigned char receiveBuffer[MAX_PACKET_LENGTH];
    size_t headerDataSize;
    ssize_t receivedDataSize;
    createConnPacket(mode).makeData(sendHeader, headerDataSize);
    timeval tv;
    fd_set fd;
    int attempt;
    for (attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
        tv.tv_sec = TIMEOUT_SEC;
        tv.tv_usec = TIMEOUT_USEC;
        FD_ZERO(&fd);
        FD_SET(socket, &fd);
        cout << "SEND " << sendto(socket, sendHeader, headerDataSize, 0, ai->ai_addr, ai->ai_addrlen) << ", pokus  #"
             << attempt + 1 << endl;
        RECV:
        if (select(socket + 1, &fd, NULL, NULL, &tv) > 0) {
            cout << "RECV " << (receivedDataSize = recvfrom(socket, receiveBuffer, MAX_PACKET_LENGTH, 0, ai->ai_addr,
                                                            &ai->ai_addrlen)) << endl;
            if (receivedDataSize < MIN_PACKET_LENGTH) {
                goto RECV;
            }
            dataPacket x;
            x.parseData(receiveBuffer, receivedDataSize);
            if (x.m_symptom != SYM_SYN || x.m_secvID != 0 || x.m_confirmID != 0 || x.m_dataLength > 1 ||
                x.m_data[0] != mode) {
                goto RECV;
            }
            if (x.m_connID == 0){
                sendRSTPacket(socket, ai, connID);
                return false;
            }
            connID = x.m_connID;
            break;
        }
    }
    if (attempt == MAX_ATTEMPTS) {
        cout << "TIMEOUT." << endl;
        return false;
    }
    return true;
}

void sendAckPacket(int socket, addrinfo *ai, uint32_t connID, uint16_t last) {
    unsigned char buffer[MAX_PACKET_LENGTH];
    size_t len;
    createAckPacket(connID, last).makeData(buffer, len);
    sendto(socket, buffer, len, 0, ai->ai_addr, ai->ai_addrlen);
}


int main(int argc, char *argv[]) {
    int socket;
    if (argc < 2) {
        cout << "Malo parametru." << endl;
        return 1;
    } else if (argc > 4) {
        cout << "Prilis mnoho parametru." << endl;
        return 1;
    }
    struct addrinfo *ai;
    if ((socket = createUDPSocket(argv[1], to_string(PORT), ai)) == -1) {
        cout << "Nepovedlo se vytvořit spojení." << endl;
        freeaddrinfo(ai);
        return 1;
    }
    uint32_t connID;
    if (!initiateConnection(socket, ai, DOWNLOAD, connID)) {
        return 1;
    }
    cout << connID << endl;
    uint16_t last = 0;
    dataPacket x;
    ofstream of("foto.png", ofstream::out | ofstream::binary);
    set<dataPacket> savedData;
    uint8_t sameLastCnt = 0;
    while (true) {
        unsigned char buffer[MAX_PACKET_LENGTH];
        ssize_t received;
        timeval tv;
        tv.tv_sec = 0;
        tv.tv_usec = 200000;
        fd_set fd;
        FD_ZERO(&fd);
        FD_SET(socket, &fd);
        if (select(socket + 1, &fd, NULL, NULL, &tv) > 0) {
            received = recvfrom(socket, buffer, MAX_PACKET_LENGTH, 0, ai->ai_addr, &ai->ai_addrlen);
            x.parseData(buffer, received);
            if (x.m_connID != connID) {
                cout << "Packet nekoho jineho." << endl;
                continue;
            }
            if (x.m_symptom != 0) {
                if (x.m_symptom == SYM_SYN) {
                    sendRSTPacket(socket, ai, connID);
                    break;
                }
                if (x.m_symptom == SYM_FIN) {
                    cout << "Koncime!" << endl;
                    if (x.m_secvID != last) {
                        sendRSTPacket(socket, ai, connID);
                    }
                    size_t len;
                    dataPacket(connID, 0, x.m_secvID, SYM_FIN).makeData(buffer, len);
                    sendto(socket, buffer, len, 0, ai->ai_addr, ai->ai_addrlen);
                    break;
                }
                if (x.m_symptom == SYM_RST) {
                    cout << "Nekde nastala chyba." << endl;
                    break;
                }
                cout << "Chyba" << endl;
                sendRSTPacket(socket, ai, connID);
                break;
            }
            cout << "RECV SECV " << x.m_secvID << endl;
            sameLastCnt++;
            if (x.m_secvID == last) {
                of.write((char *) x.m_data, x.m_dataLength);
                last += x.m_dataLength;
                sameLastCnt = 0;
                while (true) {
                    /* Pokus o nalezeni dalsiho packetu v poradi */
                    auto it = savedData.find(dataPacket(0, last, 0, 0));
                    if (it != savedData.end() && it->m_secvID == last) {
                        of.write((char *) it->m_data, it->m_dataLength);
                        of.flush();
                        last += it->m_dataLength;
                        savedData.erase(it);
                    } else break;
                }
            } else {
                /* ulozeni do fronty, pokud jiz je ulozen, nic se nestane */
                if(savedData.insert(x).second) sameLastCnt = 0;
            }
            /*if (sameLastCnt > 20){
                cout<<"20x nepřišlo nic nového. Chyba"<<endl;
                sendRSTPacket(socket, ai, connID);
                break;
            }*/
            cout << "SEND ACK " << last << endl;
            sendAckPacket(socket, ai, connID, last);
        } else {
            cout << "TIMEOUT" << endl;
            break;
        }
    }
    freeaddrinfo(ai);
    close(socket);
    return 0;
}